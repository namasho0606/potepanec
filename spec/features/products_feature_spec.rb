RSpec.feature 'ProductsFeatures', type: :feature do
  given(:taxonomy) { create(:taxonomy, name: 'Category') }
  given(:taxon) { create(:taxon, name: 'Mugs', taxonomy: taxonomy, parent: taxonomy.root) }
  given!(:product) { create(:product, taxons: [taxon], name: 'Ruby on Rails Stein') }
  given!(:product2) { create(:product, taxons: [taxon], name: 'products2') }
  given!(:product3) { create(:product, taxons: [taxon], name: 'products3') }
  given!(:product4) { create(:product, taxons: [taxon], name: 'products4') }
  given!(:product5) { create(:product, taxons: [taxon], name: 'products5') }

  scenario '商品詳細ページからカテゴリーページにアクセスできること' do
    visit potepan_product_path(product.id)

    expect(page).to have_content(product.name, count: 3)
    expect(page).to have_content(product.description)
    expect(page).to have_content(product.display_price.to_s)
    click_link '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(taxon.id)
    expect(page).to have_title 'Mugs - BIGBAG'
  end

  scenario '商品詳細ページから関連商品の詳細ページにアクセスできること' do
    visit potepan_product_path(product.id)

    expect(page).to have_content(product.name, count: 3)
    expect(page).to have_content(product.description)
    expect(page).to have_content(product.display_price.to_s)
    within '.productsContent' do
      expect(page).not_to have_content product.name
      expect(page).to have_content product2.name
      expect(page).to have_content product3.name
      expect(page).to have_content product4.name
      expect(page).to have_content product5.name
      expect(page).to have_content product2.display_price.to_s
      expect(page).to have_content product3.display_price.to_s
      expect(page).to have_content product4.display_price.to_s
      expect(page).to have_content product5.display_price.to_s
      click_link product5.name
    end
    expect(current_path).to eq potepan_product_path(product5.id)
    expect(page).to have_title product5.name
  end
end
