RSpec.feature 'CategoriesFeatures', type: :feature do
  given(:taxonomy) { create(:taxonomy, name: 'Category') }
  given(:taxon) { create(:taxon, name: 'Mugs', taxonomy: taxonomy, parent: taxonomy.root) }
  given!(:product) { create(:product, taxons: [taxon], name: 'Ruby on Rails Stein') }
  given!(:another_taxon) { create(:taxon, name: 'Bag', taxonomy: taxonomy, parent: taxonomy.root) }
  given!(:another_product) { create(:product, taxons: [taxon], name: 'Ruby on Rails Mug') }
  given!(:another_product2) { create(:product, taxons: [another_taxon], name: 'Ruby on Rails Bag') }

  scenario 'カテゴリーページにアクセスできること' do
    visit potepan_category_path(taxonomy.root.id)

    expect(page).to have_title 'Category - BIGBAG'
    within '.side-nav' do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content taxon.name
      expect(taxon.products.count).to eq(2)
      click_link taxon.name
    end
    expect(page).to have_title 'Mugs - BIGBAG'
  end

  scenario '特定のカテゴリの商品が表示できること' do
    visit potepan_category_path(taxon.id)

    expect(page).to have_title 'Mugs - BIGBAG'
    expect(page).to have_content product.name
    expect(page).to have_content another_product.name
    expect(page).not_to have_content another_product2.name
  end

  scenario '商品詳細ページにアクセスできること' do
    visit potepan_category_path(taxon.id)

    click_link product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end
end
