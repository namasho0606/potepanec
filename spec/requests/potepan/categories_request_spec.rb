RSpec.describe 'Potepan::Categories', type: :request do
  describe 'potepan/category' do
    let(:taxonomy) { create(:taxonomy, name: 'Category') }
    let(:taxon) { create(:taxon, name: 'Bag', taxonomy: taxonomy, parent: taxonomy.root) }
    let!(:product) { create(:product, taxons: [taxon], name: 'Ruby on Rails Bag') }

    before do
      get potepan_category_path(taxon.id)
    end

    it '正常にレスポンスを返すこと' do
      expect(response).to have_http_status 200
    end

    it 'レスポンスにtaxonomy.nameが含まれていること' do
      expect(response.body).to include taxonomy.name
    end

    it 'レスポンスにtaxon.nameが含まれていること' do
      expect(response.body).to include taxon.name
    end

    it 'レスポンスにtaxon.produts.countが含まれていること' do
      expect(response.body).to include taxon.products.count.to_s
    end
  end
end
