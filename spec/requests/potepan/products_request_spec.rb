RSpec.describe 'Potepan::Products', type: :request do
  describe 'potepan/products' do
    let(:taxonomy) { create(:taxonomy, name: 'Category') }
    let(:taxon) { create(:taxon, name: 'Bag', taxonomy: taxonomy, parent: taxonomy.root) }
    let!(:product) { create(:product, taxons: [taxon], name: 'Ruby on Rails Bag') }
    let!(:product2) { create(:product, taxons: [taxon], name: 'products2') }
    let!(:product3) { create(:product, taxons: [taxon], name: 'products3') }
    let!(:product4) { create(:product, taxons: [taxon], name: 'products4') }
    let!(:product5) { create(:product, taxons: [taxon], name: 'products5') }

    before do
      get potepan_product_path(product.id)
    end

    it '正常にレスポンスを返すこと' do
      expect(response).to have_http_status 200
    end

    it 'レスポンスに商品説明が含まれていること' do
      expect(response.body).to include product.description
    end

    it 'レスポンスに商品名が含まれていること' do
      expect(response.body).to include product.name
    end

    it 'レスポンスに商品価格が含まれていること' do
      expect(response.body).to include product.display_price.to_s
    end

    it 'レスポンスに関連商品名が含まれていること' do
      expect(response.body).to include product2.name
      expect(response.body).to include product3.name
      expect(response.body).to include product4.name
      expect(response.body).to include product5.name
    end

    it 'レスポンスに関連商品価格が含まれていること' do
      expect(response.body).to include product2.display_price.to_s
      expect(response.body).to include product3.display_price.to_s
      expect(response.body).to include product4.display_price.to_s
      expect(response.body).to include product5.display_price.to_s
    end
  end
end
