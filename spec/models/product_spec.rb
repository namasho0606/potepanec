RSpec.describe Spree::Product, type: :model do
  describe 'def relation_products' do
    let(:taxonomy) { create(:taxonomy, name: 'Category') }
    let(:taxonomy2) { create(:taxonomy, name: 'Machine') }
    let(:taxon) { create(:taxon, name: 'Bag', taxonomy: taxonomy, parent: taxonomy.root) }
    let(:taxon2) { create(:taxon, name: 'Car', taxonomy: taxonomy2, parent: taxonomy2.root) }
    let!(:product) { create(:product, taxons: [taxon], name: 'Ruby on Rails Bag') }
    let!(:product2) { create(:product, taxons: [taxon], name: 'products2') }
    let!(:product3) { create(:product, taxons: [taxon], name: 'products3') }
    let!(:product4) { create(:product, taxons: [taxon], name: 'products4') }
    let!(:product5) { create(:product, taxons: [taxon], name: 'products5') }
    let!(:product6) { create(:product, taxons: [taxon2], name: 'Jimny') }
    let(:count) { Settings.ProductDecorator.PRODUCTS_MAX_COUNTS }

    it 'product.relation_productsが正しい関連商品を４つ返すこと' do
      expect(product.relation_products(count)).to match_array [
        product2,
        product3,
        product4,
        product5,
      ]
    end
  end
end
