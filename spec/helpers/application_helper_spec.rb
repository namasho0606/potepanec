RSpec.describe ApplicationHelper, type: :helper do
  describe 'full_title(page_title)' do
    context 'page_title = emptyの時' do
      it 'BIGBAG Store　であること' do
        expect(full_title('')).to eq 'BIGBAG Store'
      end
    end

    context 'page_title = nilの時' do
      it 'BIGBAG Store　であること' do
        expect(full_title(nil)).to eq 'BIGBAG Store'
      end
    end

    context 'page_title = 文字列の時' do
      it '文字列 - BIGBAG Store　であること' do
        expect(full_title('rails')).to eq 'rails - BIGBAG Store'
      end
    end
  end
end
