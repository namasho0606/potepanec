module Spree::ProductDecorator
  def relation_products(products_max_counts)
    Spree::Product.joins(:taxons).
      includes(master: [:images, :default_price]).
      where(spree_products_taxons: { taxon_id: taxon_ids }).
      where.not(id: id).
      distinct.
      limit(products_max_counts)
  end

  Spree::Product.prepend self
end
