class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.relation_products(Settings.ProductDecorator.PRODUCTS_MAX_COUNTS)
  end
end
